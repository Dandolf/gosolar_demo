import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Proposal from '@/components/tabs/Proposal'
import CreditCheck from '@/components/tabs/CreditCheck'
import LoanDocs from '@/components/tabs/LoanDocs'
import Dcp from '@/components/tabs/Dcp'
import Led from '@/components/tabs/LedLights'
import Nest from '@/components/tabs/NestThermostat'
import SiteSurvey from '@/components/tabs/SiteSurvey'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/proposal',
      name: 'Proposal',
      component: Proposal
    },
    {
      path: '/creditcheck',
      name: 'CreditCheck',
      component: CreditCheck
    },
    {
      path: '/loandocs',
      name: 'LoanDocs',
      component: LoanDocs
    },
    {
      path: '/dcp',
      name: 'Dcp',
      component: Dcp
    },
    {
      path: '/led',
      name: 'Led',
      component: Led
    },
    {
      path: '/nest',
      name: 'Nest',
      component: Nest
    },
    {
      path: '/sitesurvey',
      name: 'SiteSurvey',
      component: SiteSurvey
    }
  ],
  mode: 'history'
})
