import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        boltData: {
            Project: {
                firstName: 'Bob',
                lastName: 'Boberson',
                address: '123 ST',
                city: 'Provo',
                state: 'UT',
                zip: '12345',
                phone: '(123) 456-6789',
                email: 'bob@cool.com'
            },
            Financer: {
                name: 'GreenSky'
            },
            SalesRep: {
                firstName: 'Joe',
                lastName: 'Shmoe',
                email: 'salesGuy@sales.com'
            }
        },
        user: {
            id: 'ksdfkljosidf'
        }
    },
    mutations: {

    },
    actions: {

    },
    getters: {
        boltData (state) {
            return state.boltData;
        },
        boltDataCopy (state) {
            return JSON.parse(JSON.stringify(state.boltData))
        }
    }
})